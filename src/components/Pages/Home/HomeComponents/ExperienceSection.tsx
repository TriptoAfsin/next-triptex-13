"use client";
import {
  Box,
  Button,
  Grid,
  Heading,
  Image,
  Text,
  useColorMode,
} from "@chakra-ui/react";
import dayjs from "dayjs";
import React from "react";

function ExperienceSection() {
  const { colorMode } = useColorMode();

  let expArr = [
    {
      id: 21212,
      title: "REDQ",
      logo: "/redq.svg",
      time: "(April, 2024 - *)",
      start: "2024-4-29",
      end: Date?.now(),
      designation: "Software Engineer",
      body: `REDQ is a creative agency specializing in building scalable, high performance web & mobile application`,
      link: "https://redq.io/",
      gradColor1: "#000000",
      gradColor2: "#2d2f33",
      tags: ["NextJS", "TypeScript", "NodeJs", "Redis"],
    },
    {
      id: 1,
      title: "Edison Communication Ltd.",
      logo: "/edison-cl.svg",
      time: "(Jun, 2023 - April,2024)",
      start: "2023-6-5",
      end: "2024-4-30",
      designation: "Frontend Developer",
      body: `I've worked as a frontend developer at "Edison Communication Ltd." since June, 2023. I've been working on numerous projects here, mostly WebManza, which is a no code website building platform`,
      link: "https://www.webmanza.com/",
      gradColor1: "#12379f",
      gradColor2: "#05006a",
      tags: ["NextJS", "React", "RTK", "Git"],
    },
    {
      id: 2,
      title: "Brac SDP",
      logo: "/brac-sdp.png",
      time: "(April, 2023 - Dec,2023)",
      start: "2023-4-1",
      end: "2023-12-31",
      designation: "Project Partner",
      body: `Brac SDP is a HRM solution for empowering unemployed youths, I'm responsible for building the whole frontend part`,
      link: "https://dev-admin.somadhanhobe.com/",
      gradColor1: "#e50389",
      gradColor2: "#e50389",
      tags: ["NextJS", "React Query", "Material", "Git"],
    },
    {
      id: 3,
      title: "Delivery Hobe",
      logo: "/dh.png",
      time: "(2021 - 2023)",
      start: "2021-1-10",
      end: "2023-6-5",
      designation: "JavaScript Developer",
      body: `I've worked as a JavaScript developer at "Delivery Hobe" since October, 2021 to June, 2023,  It's a hyper local e-commerce platform focused on 1hr deliveries within Dhaka city`,
      link: "https://www.deliveryhobe.com/",
      gradColor1: "#f9553e",
      gradColor2: "#fe9909",
      tags: ["NextJS", "React", "Redux", "Git"],
    },
    {
      id: 4,
      title: "BUTEX NoteBOT",
      logo: "/bot.png",
      time: "(2018 - ?)",
      designation: "Founder",
      start: "2018-1-9",
      end: Date?.now(),
      body: `I've founded BUTEX NoteBOT in 2018, it's a 24x7 educational content delivery chatbot.Through this time it has served thousands of students with their studies`,
      link: "https://github.com/TriptoAfsin/notebot-engine-v1",
      gradColor1: "#23a455",
      gradColor2: "#3bad67",
      tags: ["NodeJS", "MySQL", "Git"],
    },
    {
      id: 5,
      title: "Bondi Pathshsala",
      logo: "/bp.png",
      time: "(2020 - 2021)",
      start: "2020-1-1",
      end: "2021-1-1",
      designation: "Web Developer",
      body: `I was responsible for maintaining the whole web platform of Bondi Pathshsala since 2020 - 2021, It has helped more than 2 million students with their studies so far`,
      link: "https://www.bondipathshala.com.bd/",
      gradColor1: "#4978f9",
      gradColor2: "#275cf1",
      tags: ["Wordpress", "cPanel", "CSS"],
    },
  ];

  type TExpCard = {
    title: string;
    id?: number | string;
    designation: string;
    logo: string;
    body: string;
    link: string;
    time: string;
    start: string;
    end: string | number;
    gradColor1: string;
    gradColor2: string;
    tags: string[];
  };

  let ExpCard: React.FC<TExpCard> = ({
    title,
    id,
    designation,
    logo,
    body,
    link,
    time,
    start,
    end,
    gradColor1 = "#4575f8",
    gradColor2 = "#275cf1",
    tags = [],
  }) => {
    const gradient = `linear(to-l, ${gradColor1}, ${gradColor2})`;
    return (
      <Box
        display={"flex"}
        flexDir={"column"}
        key={id}
        justifyContent={"center"}
        alignItems={"center"}
      >
        <Heading
          color={colorMode === "dark" ? "#fafafb" : "#fafafb"}
          borderRadius={"10px"}
          fontSize={24}
          mt={2}
          padding={4}
          bgGradient={gradient}
          width={["90vw", "100%", "100%", "100%"]}
          textAlign={"center"}
        >
          {title}
        </Heading>
        {id === 2 && (
          <Box
            minH={"110px"}
            maxH={"110px"}
            display={"flex"}
            flexDir={"column"}
            alignItems={"center"}
            justifyContent={"center"}
          >
            <Image
              src={logo}
              alt={title}
              width={"92px"}
              height={id === 2 ? "auto" : "92px"}
              mt={5}
            />
          </Box>
        )}
        {id !== 2 && (
          <Image src={logo} alt={title} width={"92px"} height={"92px"} mt={5} />
        )}
        <Text
          mt={2}
          color={colorMode === "dark" ? "#fafafb" : "#fafafb"}
          fontWeight={"bold"}
        >
          {designation}
        </Text>
        <Box display={"flex"} flexDirection={"row"} alignItems={"center"}>
          <Text
            color={colorMode === "dark" ? "#fafafb" : "#fafafb"}
            background={"black"}
            paddingX={2}
            mt={1}
          >
            {time}
          </Text>
          <Text
            color={colorMode === "dark" ? "#fafafb" : "#fafafb"}
            background={"black"}
            paddingX={2}
            mt={1}
            ml={2}
          >
            {dayjs(end)?.diff(start, "M") >= 12
              ? dayjs(end)?.diff(start, "year") > 1
                ? `${dayjs(end)?.diff(start, "year")} Years`
                : `${dayjs(end)?.diff(start, "year")} Year`
              : `${dayjs(end)?.diff(start, "M")} Months`}
          </Text>
        </Box>
        <Box
          display={"flex"}
          flexDir={"row"}
          justifyContent={"center"}
          alignItems={"center"}
          justifyItems={"center"}
          justifySelf={"center"}
        >
          <Grid
            templateColumns={[
              "repeat(3, 1fr)",
              "repeat(4, 1fr)",
              "repeat(4, 1fr)",
              "repeat(4, 1fr)",
            ]}
            gap={4}
            mt={3}
            mr={[0, 0, 5, 5]}
            mb={2}
            padding={2}
            ml={[0, 0, 2, 2]}
          >
            {tags?.map(item => (
              <Text
                fontWeight={"semibold"}
                key={item}
                paddingTop={1}
                paddingBottom={1}
                paddingLeft={2}
                paddingRight={2}
                background={"#4949f3"}
                color={"white"}
                borderRadius={10}
                textAlign={"center"}
                fontSize={14}
              >
                {item}
              </Text>
            ))}
          </Grid>
        </Box>
        <Text
          mt={1}
          color={colorMode === "dark" ? "#fafafb" : "#fafafb"}
          ml={2}
          mr={2}
          height={"120px"}
          padding={2}
          textAlign={"center"}
        >
          {body}
        </Text>
        <Button as={"a"} href={link} cursor={"pointer"} mt={5}>
          Learn More
        </Button>
      </Box>
    );
  };

  return (
    <Box
      background={colorMode === "dark" ? "#333645" : "#3b3d47"}
      paddingTop={[5, 5, 5, 10]}
      paddingBottom={[5, 5, 5, 20]}
      display={"flex"}
      flexDir={"column"}
      justifyContent={"center"}
      alignItems={"center"}
    >
      <Heading color={colorMode === "dark" ? "#fafafb" : "#fafafb"}>
        Experience
      </Heading>
      <Grid
        templateColumns={[
          "repeat(1, 1fr)",
          "repeat(2, 1fr)",
          "repeat(2, 1fr)",
          "repeat(3, 1fr)",
        ]}
        gap={[5, 5, 10, 50]}
        mt={5}
        mr={[0, 0, 5, 5]}
        mb={5}
        padding={2}
        ml={[0, 0, 2, 2]}
        alignItems={"center"}
        justifyContent={"center"}
        justifyItems={"center"}
      >
        {expArr.map(item => (
          <ExpCard
            key={item.id}
            title={item.title}
            designation={item.designation}
            logo={item.logo}
            body={item.body}
            id={item.id}
            link={item.link}
            time={item.time}
            start={item.start}
            end={item.end}
            gradColor1={item.gradColor1}
            gradColor2={item.gradColor2}
            tags={item?.tags}
          />
        ))}
      </Grid>
    </Box>
  );
}

export default ExperienceSection;
