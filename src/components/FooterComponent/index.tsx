import React from "react";
import {
  Box,
  Heading,
  Text,
  Grid,
} from "@/components/ChakraComponents/Client";
import { SocialIcon } from "react-social-icons";
//config import
import portfolioInfo from "@/config";
let date = new Date();
let currentYear = date.getFullYear();

const githubUrl = portfolioInfo.socialLinks.socialGithub;
const facebookUrl = portfolioInfo.socialLinks.socialFacebook;
const LikedinUrl = portfolioInfo.socialLinks.socialLinkedin;
const mailUrl = portfolioInfo.socialLinks.socialMail;

function FooterComponent({ grad1Color = "#339af0", grad2Color = "#4949f3" }) {

  // const gradient = `linear(to-l, ${grad1Color}, ${grad2Color})`;
  return (
      <Box display={'flex'} flexDir={'column'} bg={'#375eec'} padding={20}>
      <Box
        className="footer-class"
        padding={10}
        key={'footer-container'}
        color={"#ffffff"}
        justifyContent={['center','center','center','space-around']}
      >
        
        <Box display={"flex"} flexDir={"column"} padding={5}>
          <Heading fontSize={[18,18,22,24]} fontWeight={600}>Afshin Nahian Tripto</Heading>
          <Text mt={2}>✅ Be the change</Text>
          <Text>
            All rights reserved by: Afshin Nahian Tripto {currentYear}
          </Text>
        </Box>
        <Box display={"flex"} flexDir={"column"} padding={5}>
          <Text fontSize={16} fontWeight={'bold'} my={4}>🏡 Address</Text>
          <Text>Dhaka, Bangladesh</Text>
        </Box>
        <Box display={['none','none','none',"flex"]} flexDir={"column"} mb={5} padding={5}>
          <Text fontSize={16} fontWeight={'bold'}>👀 Follow Me</Text>
          <Box display={"flex"} flexDir={"row"} my={4}>
            <SocialIcon
              url={githubUrl}
              bgColor={"#ffffff"}
              style={{ marginRight: 10 }}
            />
            <SocialIcon
              url={LikedinUrl}
              bgColor={"#ffffff"}
              style={{ marginRight: 15 }}
            />
            <SocialIcon
              url={facebookUrl}
              bgColor={"#ffffff"}
              style={{ marginRight: 15 }}
            />
            <SocialIcon
              url={mailUrl}
              bgColor={"#ffffff"}
              style={{ marginRight: 15 }}
            />
          </Box>
        </Box>
      </Box>
      <Text textAlign={'center'} mb={5} color={"#ffffff"} mt={20}> Made with 💝 by Afshin Nahian Tripto</Text>
      </Box>
    
  );
}

export default FooterComponent;
